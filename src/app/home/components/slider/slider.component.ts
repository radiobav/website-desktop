import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit, AfterViewInit {
  
  @Input() sliderImages: string[];

  constructor(
  ) {
  }

  slideConfig = {
    "slidesToShow": 1, 
    "slidesToScroll": 1,
    "autoplay": true,
    "adaptiveHeight": true,
    "autoplaySpeed": 4000,
    "arrwos": true,
    "lazyload": 'ondemand'
  };


  ngAfterViewInit() {
  }

  ngOnInit() {
  }

}
