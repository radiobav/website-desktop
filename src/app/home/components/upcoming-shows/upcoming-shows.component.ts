import { Component, OnInit, Input } from '@angular/core';
import { Show } from 'src/app/interfaces/show';

@Component({
  selector: 'app-upcoming-shows',
  templateUrl: './upcoming-shows.component.html',
  styleUrls: ['./upcoming-shows.component.css']
})
export class UpcomingShowsComponent implements OnInit {
  
  @Input() shows: Show[];
  fallbackImage: string;
  showAmountDisplayed: number = 3
  
  constructor() { }

  ngOnInit() {
    this.fallbackImage = '/assets/fallback/fallback.jpg'

  }

}
