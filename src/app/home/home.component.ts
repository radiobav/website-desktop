import { Component, OnInit } from '@angular/core';
import { ShowsServiceService } from '../services/shows-service.service';
import { SliderImageService } from '../services/slider-image.service';

import { Show } from '../interfaces/show';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  upcommingShows: Show[];
  sliderImages: string[];

  constructor(
    private showService: ShowsServiceService,
    private sliderImageService: SliderImageService
  ) { }

  getUpcomingShows(amount: number): void {
    this.showService.getUpcommingShows(amount)
      .then(res => this.upcommingShows = res);
  }

  getSliderImages(): void {
    this.sliderImageService.getImagesUrl()
      .then( res => this.sliderImages = res);
  }

  ngOnInit() {
    this.getUpcomingShows(9)
    this.getSliderImages();
  }

}
