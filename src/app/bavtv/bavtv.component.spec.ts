import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BavtvComponent } from './bavtv.component';

describe('BavtvComponent', () => {
  let component: BavtvComponent;
  let fixture: ComponentFixture<BavtvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BavtvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BavtvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
