import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ShowsServiceService } from '../services/shows-service.service';
import { Show } from '../interfaces/show';

@Component({
  selector: 'app-webplayer',
  templateUrl: './webplayer.component.html',
  styleUrls: ['./webplayer.component.css']
})
export class WebplayerComponent implements OnInit, AfterViewInit {

  player: HTMLAudioElement;
  canPlayWebstream: boolean;
  isPlaying: boolean;
  isLoading: boolean;

  show : Show;



  constructor(
    private showService: ShowsServiceService,
  ) { }

  getCurrentShow(): void {
    this.showService.getCurrentShow()
      .then(res => this.show = res);
  }

  checkPlaybackSupport(audio: HTMLAudioElement): 'mpeg' | 'ogg' | false {
    if(audio.canPlayType('audio/mpeg')) {
      return 'mpeg';
    }

    if(audio.canPlayType('audio/ogg')) {
      return 'ogg';
    }

    return false;
  }

  initPlayer(audio: HTMLAudioElement): HTMLAudioElement {

    if (this.checkPlaybackSupport(audio) === 'ogg') {
      return new Audio('https://stream.radiobav.de/_a')
    }

    if (this.checkPlaybackSupport(audio) === 'mpeg') {
      return new Audio('https://stream.radiobav.de/_a')
    }

    this.canPlayWebstream = false;
    return undefined;
  }

  bindPlayerEvents(): void {
    this.player.addEventListener('playing', (event) => {
        this.isPlaying = true;
    });
    this.player.addEventListener('pause', (event) => {
        this.isPlaying = false;
    });
    this.player.addEventListener('ended', (event) => {
        this.isPlaying = false;
    });
    this.player.addEventListener('loadstart', (event) => {
        this.isLoading = true;
    });
    this.player.addEventListener('loadeddata', (event) => {
        this.isLoading = false;
    });
  };


  playBtnHandler(): void {
    // If loading a song prevent spamming play button;
    if(this.isLoading) {
        return;
    }

    if( this.player.paused ) {
        this.player.play();
    } else {
        this.player.pause();
    }
  };

  ngAfterViewInit() {
    this.player = this.initPlayer(new Audio());
    this.bindPlayerEvents()
  }

  ngOnInit() {
    this.show = undefined;
    this.canPlayWebstream = true;
    this.getCurrentShow();
  }

}
