import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';

import { HttpsPipe } from './pipes/https.pipe';

import { ImgFallbackModule } from 'ngx-img-fallback';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ShowplanComponent } from './showplan/showplan.component';
import { UpcomingShowsComponent } from './home/components/upcoming-shows/upcoming-shows.component';
import { SliderComponent } from './home/components/slider/slider.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SingelShowComponent } from './showplan/components/singel-show/singel-show.component';
import { EmpfangComponent } from './empfang/empfang.component';
import { WebplayerComponent } from './webplayer/webplayer.component';
import { BavtvComponent } from './bavtv/bavtv.component';
import { FilterShowArrayPipe } from './pipes/fiterShowArray';
import { DatenschutzComponent } from './datenschutz/datenschutz.component';
import { ImpressumComponent } from './impressum/impressum.component';
import { FreundeComponent } from './freunde/freunde.component';

registerLocaleData(localeDe);
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ShowplanComponent,
    UpcomingShowsComponent,
    SliderComponent,
    NavigationComponent,
    SingelShowComponent,
    EmpfangComponent,
    WebplayerComponent,
    BavtvComponent,
    HttpsPipe,
    FilterShowArrayPipe,
    DatenschutzComponent,
    ImpressumComponent,
    FreundeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ImgFallbackModule,
    SlickCarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
