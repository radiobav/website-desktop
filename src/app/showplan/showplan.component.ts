import { Component, OnInit } from '@angular/core';
import { Show } from '../interfaces/show';
import { WeekInfo } from '../interfaces/weekinfo';
import { ShowsServiceService } from '../services/shows-service.service';
@Component({
  selector: 'app-showplan',
  templateUrl: './showplan.component.html',
  styleUrls: ['./showplan.component.css']
})
export class ShowplanComponent implements OnInit {
  shows: WeekInfo;
  date = new Date();

  constructor(
    private showService: ShowsServiceService
  ) { }

  Date (date: string) {
    return new Date(date);
  }


  getShowsOfWeek(): void {
    this.showService.getSchedule()
      .then(res => this.shows = res);
  }

  ngOnInit() {
    this.shows = undefined;
    this.getShowsOfWeek()
  }

}
