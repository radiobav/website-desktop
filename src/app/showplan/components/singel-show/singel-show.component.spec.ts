import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingelShowComponent } from './singel-show.component';

describe('SingelShowComponent', () => {
  let component: SingelShowComponent;
  let fixture: ComponentFixture<SingelShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingelShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingelShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
