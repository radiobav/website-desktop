import { Component, OnInit, Input } from '@angular/core';
import { Show } from 'src/app/interfaces/show';

@Component({
  selector: 'app-singel-show',
  templateUrl: './singel-show.component.html',
  styleUrls: ['./singel-show.component.css']
})
export class SingelShowComponent implements OnInit {

  @Input() show: Show;
  fallbackImage: string;

  constructor() { }

  ngOnInit() {
    this.fallbackImage = '/assets/fallback/fallback.jpg'

  }

}
