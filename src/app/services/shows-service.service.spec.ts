import { TestBed } from '@angular/core/testing';

import { ShowsServiceService } from './shows-service.service';

describe('ShowsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShowsServiceService = TestBed.get(ShowsServiceService);
    expect(service).toBeTruthy();
  });
});
