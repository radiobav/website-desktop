import { TestBed } from '@angular/core/testing';

import { FreundeService } from './freunde.service';

describe('FreundeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FreundeService = TestBed.get(FreundeService);
    expect(service).toBeTruthy();
  });
});
