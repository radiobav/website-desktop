import { TestBed } from '@angular/core/testing';

import { SliderImageService } from './slider-image.service';

describe('SliderImageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SliderImageService = TestBed.get(SliderImageService);
    expect(service).toBeTruthy();
  });
});
