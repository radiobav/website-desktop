import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ShowplanComponent } from './showplan/showplan.component';
import { EmpfangComponent } from './empfang/empfang.component';
import { BavtvComponent } from './bavtv/bavtv.component';
import { DatenschutzComponent } from './datenschutz/datenschutz.component';
import { ImpressumComponent } from './impressum/impressum.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'showplan',
    component: ShowplanComponent
  },
  {
    path: 'bavtv',
    component: BavtvComponent
  },
  {
    path: 'info',
    component: EmpfangComponent
  },
  {
    path: 'datenschutz',
    component: DatenschutzComponent
  },
  {
    path: 'impressum',
    component: ImpressumComponent
  },
  {
    path: '**',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
