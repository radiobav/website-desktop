import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpfangComponent } from './empfang.component';

describe('EmpfangComponent', () => {
  let component: EmpfangComponent;
  let fixture: ComponentFixture<EmpfangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpfangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpfangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
